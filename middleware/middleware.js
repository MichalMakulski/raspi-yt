const cors = (origin = '*', verbs = ['GET']) => (req, res, next) => {
  res.set({
    'Access-Control-Allow-Origin': origin,
    'Access-Control-Allow-Methods': verbs.join(',')
  });

  next();
}

module.exports = {
  cors
}