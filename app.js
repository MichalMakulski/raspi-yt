const app = require('express')();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const player = require('./routes/player');
const songs = require('./routes/songs');

const { cors } = require('./middleware/middleware');

app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());

// routes:
app.use('/player', player);
app.use('/songs', songs);

module.exports = app;