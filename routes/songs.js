const {get} = require('https');
const songs = require('express').Router();
const songsUrl = require('../config')['songs-list-url'];

songs.get('/', (req, res) => {
  get(songsUrl, (songsRes) => {
    songsRes.pipe(res);
  });
});

module.exports = songs;