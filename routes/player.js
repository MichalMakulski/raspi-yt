const player = require('express').Router();
const Mpv = require('node-mpv');
const mpv = new Mpv({audio_only: true});
// const mpv = {
//   load: ()=>{},
//   play: ()=>{},
//   pause: ()=>{},
//   stop: ()=>{},
//   volume: ()=>{}
// }

const actionRoute = '/:action/:param';

const {
  checkPlayerAction,
  setVolume,
  applyPlayerAction
} = require('./player-middleware/player-middleware');

player.use(actionRoute, checkPlayerAction);
player.use(actionRoute, setVolume);
player.get(actionRoute, applyPlayerAction(mpv));
player.use(actionRoute, (err, req, res, next) => {
  if (err.playerError) {
    return res.status(501).send({
      error: err.message
    });
  }
  res.status(500).send({
    error: 'Internal server error.'
  });
});

module.exports = player;