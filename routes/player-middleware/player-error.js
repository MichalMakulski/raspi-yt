class PlayerError extends Error {
  constructor(message) {
    super(message);

    this.playerError = true;
  }
}

module.exports = PlayerError;