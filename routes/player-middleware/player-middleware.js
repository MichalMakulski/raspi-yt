const PlayerError = require('./player-error');
const ytUrl = require('../../config')['yt-url'];

const checkPlayerAction = (req, res, next) => {
  const { action } = req.params;
  const availableActions = ['load', 'play', 'pause', 'stop', 'volume'];

  if (!availableActions.includes(action)) throw new PlayerError(`Action "${action}" not supported.`);

  next();
};

const setVolume = (req, res, next) => {
  const { action, param } = req.params;
  let volume = parseInt(param);

  if (action !== 'volume') return next();

  if (Number.isNaN(volume)) {
    throw new PlayerError('Volume level must be a number from 0 to 100.');
  }

  if (volume > 100) {
    volume = 100;
  }

  if (volume < 0) {
    volume = 0;
  }

  res.locals.volume = volume;

  next();
};

const applyPlayerAction = (player) => (req, res) => {
  const { action, param } = req.params;

  if (!player[action]) throw new PlayerError('Something went wrong.');

  switch(action) {
    case 'load':
      player.load(`${ytUrl}${param}`);
      break;
    case 'volume':
      player.volume(res.locals.volume);
      break;
    default:
      player[action]();
  }

  res.send({
    status: 'OK',
    ...req.params
  });
};

module.exports = {
  checkPlayerAction,
  setVolume,
  applyPlayerAction
}